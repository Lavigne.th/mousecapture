﻿using MouseTracker.Tracker;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MouseTracker
{
    public partial class Form1 : Form
    {
        MouseTracker.Tracker.MouseManager MouseManager = new Tracker.MouseManager();

        private const int MAX_INTERVAL = 1500;

        public Form1()
        {
            InitializeComponent();
            MouseManager.MousePositionChanged += OnMousePositionChanged;
            MouseManager.CurrentMovementLengthChanged += OnCurrentMovementLengthChanged;
            MouseManager.Start();
        }

        private void OnCurrentMovementLengthChanged(int nbMovements, bool isCapture)
        {
            this.BeginInvoke((Action)(() =>
            {
                if(!isCapture)
                    LogInfo($"{nbMovements} movements captured");
                else
                    LogInfo($"{nbMovements} movements saved to {MouseManager.FileName}");
            }));
        }

        private void OnMousePositionChanged(MousePosition mousePosition)
        {
            this.BeginInvoke((Action)(() =>
            {
                labelMousePosition.Text = $"Mouse position : {mousePosition.X}, {mousePosition.Y}";
            }));
        }

        private void OnCaptureIntervalChanged(object sender, EventArgs e)
        {
            int res = -1;
            if(int.TryParse(TbCaptureInterval.Text, out res))
            {
                if(res > MAX_INTERVAL)
                {
                    res = MAX_INTERVAL;
                    LogDanger($"Cature interval changed to {res} milliseconds");
                } else
                {
                    LogInfo($"Cature interval changed to {res} milliseconds");
                }
                MouseManager.CaptureMilli = res;
                
            }
        }

        private void OnThresholdChanged(object sender, EventArgs e)
        {
            int res = -1;
            if (int.TryParse(TbEndThreshold.Text, out res))
            {
                MouseManager.ThresholdEndMilli = res;
                LogInfo($"End movement threshold changed to {MouseManager.ThresholdEndMilli} milliseconds");
            }
        }

        private void OnTackMouseChanged(object sender, EventArgs e)
        {
            if(CbRecordMouse.Checked)
            {
                if(MouseManager.ThresholdEndMilli <= MouseManager.CaptureMilli)
                {
                    LogError("Capture milli must be smaller than end movment threshold.");
                    CbRecordMouse.Checked = false;
                } else
                {
                    TbEndThreshold.Enabled = false;
                    TbCaptureInterval.Enabled = false;
                    CbFollowMouse.Checked = true;
                    CbFollowMouse.Enabled = false;
                    MouseManager.IsRecording = true;
                }
            } else
            {
                TbEndThreshold.Enabled = true;
                TbCaptureInterval.Enabled = true;
                CbFollowMouse.Enabled = true;
                MouseManager.IsRecording = false;
            }
        }

        private void LogInfo(string message)
        {
            LabelLogger.Text = message;
            LabelLogger.ForeColor = Color.Green;
        }

        private void LogError(string message)
        {
            LabelLogger.Text = message;
            LabelLogger.ForeColor = Color.Red;
        }

        private void LogDanger(string message)
        {
            LabelLogger.Text = message;
            LabelLogger.ForeColor = Color.Orange;
        }
    }
}
