﻿
namespace MouseTracker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelMousePosition = new System.Windows.Forms.Label();
            this.CbFollowMouse = new System.Windows.Forms.CheckBox();
            this.CbRecordMouse = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.TbEndThreshold = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.TbCaptureInterval = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.LabelLogger = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.labelMousePosition);
            this.flowLayoutPanel1.Controls.Add(this.CbFollowMouse);
            this.flowLayoutPanel1.Controls.Add(this.CbRecordMouse);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel2);
            this.flowLayoutPanel1.Controls.Add(this.flowLayoutPanel3);
            this.flowLayoutPanel1.Controls.Add(this.LabelLogger);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(-1, 1);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(410, 462);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // labelMousePosition
            // 
            this.labelMousePosition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMousePosition.AutoSize = true;
            this.labelMousePosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMousePosition.Location = new System.Drawing.Point(3, 0);
            this.labelMousePosition.Name = "labelMousePosition";
            this.labelMousePosition.Size = new System.Drawing.Size(400, 25);
            this.labelMousePosition.TabIndex = 0;
            this.labelMousePosition.Text = "Mouse position :";
            // 
            // CbFollowMouse
            // 
            this.CbFollowMouse.AutoSize = true;
            this.CbFollowMouse.Checked = true;
            this.CbFollowMouse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CbFollowMouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbFollowMouse.Location = new System.Drawing.Point(3, 28);
            this.CbFollowMouse.Name = "CbFollowMouse";
            this.CbFollowMouse.Size = new System.Drawing.Size(151, 29);
            this.CbFollowMouse.TabIndex = 1;
            this.CbFollowMouse.Text = "Follow mouse";
            this.CbFollowMouse.UseVisualStyleBackColor = true;
            // 
            // CbRecordMouse
            // 
            this.CbRecordMouse.AutoSize = true;
            this.CbRecordMouse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbRecordMouse.Location = new System.Drawing.Point(3, 63);
            this.CbRecordMouse.Name = "CbRecordMouse";
            this.CbRecordMouse.Size = new System.Drawing.Size(145, 29);
            this.CbRecordMouse.TabIndex = 2;
            this.CbRecordMouse.Text = "Track mouse";
            this.CbRecordMouse.UseVisualStyleBackColor = true;
            this.CbRecordMouse.CheckedChanged += new System.EventHandler(this.OnTackMouseChanged);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.TbEndThreshold);
            this.flowLayoutPanel2.Controls.Add(this.label1);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 98);
            this.flowLayoutPanel2.MinimumSize = new System.Drawing.Size(300, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(400, 28);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // TbEndThreshold
            // 
            this.TbEndThreshold.Location = new System.Drawing.Point(3, 3);
            this.TbEndThreshold.Name = "TbEndThreshold";
            this.TbEndThreshold.Size = new System.Drawing.Size(100, 20);
            this.TbEndThreshold.TabIndex = 0;
            this.TbEndThreshold.Text = "200";
            this.TbEndThreshold.TextChanged += new System.EventHandler(this.OnThresholdChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(109, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "End threshold (milli)";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.TbCaptureInterval);
            this.flowLayoutPanel3.Controls.Add(this.label2);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 132);
            this.flowLayoutPanel3.MinimumSize = new System.Drawing.Size(400, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(400, 28);
            this.flowLayoutPanel3.TabIndex = 4;
            // 
            // TbCaptureInterval
            // 
            this.TbCaptureInterval.Location = new System.Drawing.Point(3, 3);
            this.TbCaptureInterval.Name = "TbCaptureInterval";
            this.TbCaptureInterval.Size = new System.Drawing.Size(100, 20);
            this.TbCaptureInterval.TabIndex = 0;
            this.TbCaptureInterval.Text = "10";
            this.TbCaptureInterval.TextChanged += new System.EventHandler(this.OnCaptureIntervalChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(109, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Capture interval (milli)";
            // 
            // LabelLogger
            // 
            this.LabelLogger.AutoSize = true;
            this.LabelLogger.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLogger.Location = new System.Drawing.Point(3, 163);
            this.LabelLogger.Name = "LabelLogger";
            this.LabelLogger.Size = new System.Drawing.Size(0, 25);
            this.LabelLogger.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 461);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label labelMousePosition;
        private System.Windows.Forms.CheckBox CbFollowMouse;
        private System.Windows.Forms.CheckBox CbRecordMouse;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TextBox TbEndThreshold;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.TextBox TbCaptureInterval;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LabelLogger;
    }
}

