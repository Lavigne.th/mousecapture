﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MouseTracker.Tracker
{
    class MouseManager
    {
        private bool isRecording = false;
        public bool IsRecording {
            get => isRecording;
            set
            {
                if (value)
                    CurrentMovement.Clear();
                isRecording = value;
            }
        }

        public bool IsFollowing = true;

        public int CaptureMilli = 10;

        public int ThresholdEndMilli = 200;

        public bool Running = true;

        public List<MousePosition> CurrentMovement = new List<MousePosition>();

        public Action<MousePosition> MousePositionChanged;

        public Action<int, bool> CurrentMovementLengthChanged;

        private MousePosition LastPosition;

        public string FileName => $"capture{CaptureMilli}_{ThresholdEndMilli}.txt";

        public const string FOLDER_NAME = "captures";

        public string FullPath => $"{FOLDER_NAME}/{FileName}";

        public MouseManager()
        {

        }

        public void Start()
        {
            Task.Run(async () =>
            {
                while(Running)
                {
                    Thread.Sleep(CaptureMilli);
                    if(IsFollowing)
                    {
                        Point point = NativeCursor.GetCursorPosition();
                        MousePosition position = new MousePosition(point.X, point.Y, CurrentMovement.Count * CaptureMilli);
                        MousePositionChanged?.Invoke(position);
                        if(isRecording)
                        {
                            UpdateCurrentMovement(position);
                        }
                        LastPosition = position;
                    }
                }
            });
        }

        private void UpdateCurrentMovement(MousePosition position)
        {
            if (CurrentMovement.Count > 0 || LastPosition != null && !LastPosition.Equals(position))
            {
                CurrentMovement.Add(position);
                if (CurrentMovement.Count > 1)
                {
                    int milliWithoutMovement = GetMilliWithoutMovement(position);
                    if (milliWithoutMovement > ThresholdEndMilli)
                    {
                        SaveCurrentMovement();
                        CurrentMovement.Clear();
                    } else
                    {
                        CurrentMovementLengthChanged?.Invoke(CurrentMovement.Count, false);
                    }
                }
            }
        }

        private void SaveCurrentMovement()
        {
            while(CurrentMovement.Count > 1 && CurrentMovement[CurrentMovement.Count - 2].Equals(CurrentMovement.Last())) {
                Console.WriteLine("Removed " + CurrentMovement.Last().ToString());
                CurrentMovement.Remove(CurrentMovement.Last());
            }

            CurrentMovementLengthChanged?.Invoke(CurrentMovement.Count, true);
            if(!Directory.Exists(FOLDER_NAME))
            {
                Directory.CreateDirectory(FOLDER_NAME);
            }
            if(CurrentMovement.Count > 3)
            {
                string line = "";
                for (int i = 0; i < CurrentMovement.Count; i++)
                {
                    if (i == 0)
                        line += CurrentMovement.ElementAt(i).Serialize();
                    else
                        line += $"|{CurrentMovement.ElementAt(i).Serialize()}";
                }
                using (StreamWriter writer = File.AppendText(FullPath))
                {
                    writer.WriteLine(line);
                }
            }
        }

        private int GetMilliWithoutMovement(MousePosition position)
        {
            int totalWithoutMoveMilli = 0;
            bool isDifferent = false;
            for (int i = CurrentMovement.Count - 2; i >= 0 && !isDifferent; i--)
            {
                MousePosition previousPosition = CurrentMovement.ElementAt(i);
                if (!previousPosition.Equals(position))
                {
                    isDifferent = true;
                }
                else
                {
                    totalWithoutMoveMilli += CaptureMilli;
                }
            }
            return totalWithoutMoveMilli;
        }
    }
}
