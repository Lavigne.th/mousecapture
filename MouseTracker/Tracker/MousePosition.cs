﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MouseTracker.Tracker
{
    class MousePosition
    {
        public int X;

        public int Y;

        public int TimeMilli;

        public MousePosition(int x, int y, int timeMilli)
        {
            X = x;
            Y = y;
            TimeMilli = timeMilli;
        }

        public bool Equals(MousePosition p)
        {
            if (p is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, p))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != p.GetType())
            {
                return false;
            }

            // Return true if the fields match.
            // Note that the base class is not invoked because it is
            // System.Object, which defines Equals as reference equality.
            return (X == p.X) && (Y == p.Y);
        }

        public override string ToString()
        {
            return $"[{X}, {Y}, {TimeMilli}]";
        }

        public string Serialize()
        {
            return $"{X};{Y};{TimeMilli}";
        }
    }
}
